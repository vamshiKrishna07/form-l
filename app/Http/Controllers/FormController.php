<?php

namespace App\Http\Controllers;

use App\Models\FormUpload;
use Illuminate\Http\Request;

class FormController extends Controller
{
    //
    public function form()
    {
        return view('formUpload');
    }


    public function formUpload(Request $request)
    {
        // validation
        $request->validate([
            'name' => 'required',
            'recaptcha' => 'required',
        ]);

        // Google’s verification API
        $url = 'https://www.google.com/recaptcha/api/siteverify';
        $remoteip = $_SERVER['REMOTE_ADDR'];
        $data = [
            'secret' => config('recaptcha.api_secret_key'),
            'response' => $request->get('recaptcha'),
            'remoteip' => $remoteip
        ];
        $options = [
            'http' => [
                'header' => "Content-type: application/x-www-form-urlencoded\r\n",
                'method' => 'POST',
                'content' => http_build_query($data)
            ]
        ];
        $context = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        $resultJson = json_decode($result);
        if ($resultJson->success != true) {
            return back()->withErrors(['captcha' => 'ReCaptcha Error']);
        }
        if ($resultJson->score >= 0.3) {
            //Validation was successful
            // form submission
            $formDetails = $request->all();
            // dd($formDetails);
            FormUpload::create($formDetails);
            return back()->with('message', 'Thanks for your message!');
        } else {
            return back()->withErrors(['captcha' => 'ReCaptcha Error']);
        }



        return redirect()->route('form');
    }
}
