<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    {{-- <script src="https://www.google.com/recaptcha/api.js" async defer></script> --}}

</head>

<body>
    <div class="h-screen flex justify-center items-center">
        <form action="{{ route('form.upload') }}" method="post" enctype="multipart/form-data"
            class="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4 w-96">
            @csrf
            <div class="mb-4">
                <label class="block text-gray-700 text-sm font-bold mb-2" for="name">
                    name
                </label>
                <input
                    class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                    id="name" type="text" placeholder="name" name="name">
                @if ($errors->has('name'))
                    <span class="help-block text-danger">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
                <input type="hidden" name="recaptcha" id="recaptcha">
            </div>
            <div class="mt-5">
                {{-- {!! NoCaptcha::renderJs('fr', true, 'recaptchaCallback') !!}
                {!! NoCaptcha::display() !!} --}}
            </div>
            <div class="flex items-center justify-between">
                <button type="submit"
                    class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                    type="button">
                    Sign In
                </button>
            </div>
        </form>
    </div>
</body>

<script src="https://www.google.com/recaptcha/api.js?render={{ config('recaptcha.api_site_key') }}"></script>
<script>
         grecaptcha.ready(function() {
             grecaptcha.execute('{{ config('recaptcha.api_site_key') }}', {action: 'contact'}).then(function(token) {
                if (token) {
                  document.getElementById('recaptcha').value = token;
                }
             });
         });
</script>

</html>
